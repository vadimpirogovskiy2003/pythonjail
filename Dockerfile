FROM ubuntu:22.04

RUN apt-get update && apt-get -y dist-upgrade && \
  apt-get install -y socat python3

RUN useradd -m ctf

WORKDIR /home/ctf

COPY ./src /home/ctf/src
COPY ./flag /home/ctf

RUN chown -R root:ctf /home/ctf && \
  chmod -R 755 /home/ctf && \
  chmod 744 /home/ctf/flag

USER ctf

CMD socat TCP-LISTEN:8887,reuseaddr,fork EXEC:"python3 /home/ctf/src/main.py"
